"""
Python  by Techfossguru
Copyright (C) 2017  Satish Prasad

"""
import logging
from cassandra import ConsistencyLevel
from cassandra.cluster import Cluster, BatchStatement
from cassandra.query import SimpleStatement


class PythonCassandraExample:

    def __init__(self):
        self.cluster = None
        self.session = None
        self.keyspace = None
        self.log = None

    def __del__(self):
        self.cluster.shutdown()

    def createsession(self):
        self.cluster = Cluster(['localhost'])
        self.session = self.cluster.connect(self.keyspace)

    def getsession(self):
        return self.session

    # How about Adding some log info to see what went wrong
    def setlogger(self):
        log = logging.getLogger()
        log.setLevel('INFO')
        handler = logging.StreamHandler()
        handler.setFormatter(logging.Formatter("%(asctime)s [%(levelname)s] %(name)s: %(message)s"))
        log.addHandler(handler)
        self.log = log

    # Create Keyspace based on Given Name
    def createkeyspace(self, keyspace):
        """
        :param keyspace:  The Name of Keyspace to be created
        :return:
        """
        # Before we create new lets check if exiting keyspace; we will drop that and create new
        rows = self.session.execute("SELECT keyspace_name FROM system_schema.keyspaces")
        if keyspace in [row[0] for row in rows]:
            self.log.info("dropping existing keyspace...")
            self.session.execute("DROP KEYSPACE " + keyspace)

        self.log.info("creating keyspace...")
        self.session.execute("""
                CREATE KEYSPACE %s
                WITH replication = { 'class': 'SimpleStrategy', 'replication_factor': '1' }
                """ % keyspace)

        self.log.info("setting keyspace...")
        self.session.set_keyspace(keyspace)

    def create_table(self):
        c_sql1 = """
                CREATE TABLE autors_by_usuario (usuario VARCHAR PRIMARY KEY, nombre VARCHAR, twitter VARCHAR, descripcion VARCHAR);                 """
        c_sql2 = """
                CREATE TABLE autors_by_twitter (twitter TEXT, usuario TEXT, descripcion TEXT, nombre TEXT, PRIMARY KEY (twitter,usuario)) WITH CLUSTERING ORDER BY (usuario ASC);
                 """
        c_sql3 = """
                CREATE TABLE news_by_user (usuario TEXT, titulo TEXT, cuerpo TEXT, fecha_p TEXT, tags SET<TEXT>, descripcion TEXT, PRIMARY KEY (usuario));
                 """
        c_sql4 = """
                CREATE TABLE news_by_date_range (fecha_com TIMEUUID, titulo TEXT, usuario TEXT, comantario TEXT, PRIMARY KEY (usuario));
                 """        
          
        self.session.execute(c_sql1)
        self.log.info("autors_by_usuario Table Created !!!")
        self.session.execute(c_sql2)
        self.log.info("autors_by_twitter Table Created !!!")
        self.session.execute(c_sql3)
        self.log.info("news_by_user Table Created !!!")
        self.session.execute(c_sql4)
        self.log.info("news_by_date_range Table Created !!!")

    # lets do some batch insert
    def insert_data(self):
        insert_sql = self.session.prepare("INSERT INTO  autors_by_usuario (usuario, nombre , twitter,descripcion) VALUES (?,?,?,?)")
        batch = BatchStatement()
        batch.add(insert_sql, ('frank_16', 'Francisco Ramos', '@frank_16', 'Estudiante'))
        batch.add(insert_sql, ('quilky', 'Antonio Morales', '@quilky', 'Profesor'))
        batch.add(insert_sql, ('IvanH', 'Ivan Herrera', '@IvanH','Periodista'))
        batch.add(insert_sql, ('YuliaT','Julia Gonzalez', 'YuliaT','Escritora'))
        self.session.execute(batch)
        self.log.info('Batch Insert Completed')

    def select_data(self):
        rows = self.session.execute('select * from autors_by_usuario;')
        for row in rows:
            print(row.usuario, row.nombre, row.twitter, row.descripcion)

    def update_data(self):
        pass

    def delete_data(self):
        pass


if __name__ == '__main__':
    example1 = PythonCassandraExample()
    example1.createsession()
    example1.setlogger()
    example1.createkeyspace('techfossguru')
    example1.create_table()
    example1.insert_data()
    example1.select_data()